function [ ] = generate_csv_file( review_json )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    file_id=fopen('review_full_data.csv','w');
    fprintf(file_id,'stars,review_text,cool,funny,useful\n');
    rc=size(review_json);
    for i=1:rc(2)
        if isempty(review_json{i}.text) == 1
            continue;
        end
        %stars , review_text , cool , funny , useful
        review_text='';
        review_text=remove_special_characters(review_json{i}.text);
        fprintf(file_id,'%d,%s,%d,%d,%d\n',review_json{i}.stars,...
        review_text,...
        review_json{i}.votes.cool,...
        review_json{i}.votes.funny,...
        review_json{i}.votes.useful);
    end
end

