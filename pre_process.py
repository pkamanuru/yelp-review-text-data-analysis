from stemming.porter2 import stem
from nltk.corpus import stopwords
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

#Read Data and Process and Give
cachedStopWords = stopwords.words("english")
def process_text(filename):
    reviews_dataframe=pd.read_csv(filename)
    (rows,columns)=reviews_dataframe.shape
    for i in xrange(rows):
        #Stemming
        reviews_dataframe.review_text[i]=stem(reviews_dataframe.review_text[i])
        #Stop Word Removal
        reviews_dataframe.review_text[i]=' '.join([word for word in reviews_dataframe.review_text[i].split() if word not in cachedStopWords])
        #Keeping Reviews that have no review_text as well
        reviews_dataframe = reviews_dataframe.fillna('')
    return reviews_dataframe;

def convert_to_feature_space(list_of_documents,vectorizer):
    #vectorizer=CountVectorizer(min_df=20)
    X=vectorizer.fit_transform(list_of_documents)
    #bigram_vectorizer = CountVectorizer(ngram_range=(1, 2), token_pattern=r'\b\w+\b', min_df=1)
    return X.toarray()

def print_review_text(review_data,stars,rating):
    f=open('rating.txt','w')
    i=0
    count=0
    for star in stars:
        if star == rating:
            f.write(' '+review_data[i]) 
            count = count +1
        i=i+1
    print(count)
    f.close()
