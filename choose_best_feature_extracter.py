from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression


#from clustering_visualization import plot_2d_differently_rated_reviews
colors=['b','c','y','m','r','g']
labels=['Rating 0','Rating 1','Rating 2','Rating 3','Rating 4','Rating 5']

def plot_2d_differently_rated_reviews(features_review_data, stars, pca_title):
    plt.figure()
    pca=PCA(n_components=2).fit(features_review_data)
    pca_2d=pca.transform(features_review_data)
    plt.title(pca_title) 
    #Splitting Output into x and y Coordinates
    x=list(pca_2d[:,0])
    y=list(pca_2d[:,1])
    for rating in range(1,6):
        #Subset Indices of those with stars rating
        stars_i = [review_index for review_index in range(len(stars)) if stars[review_index] == rating]
        
        #Fetching those Corresponding Points
        x_subset = [x[i] for i in stars_i]
        y_subset = [y[i] for i in stars_i]
        plt.scatter(x_subset,y_subset,color=colors[rating],label=labels[rating])
        #plot_2d_clustering_output(x_subset,y_subset,rating)
    plt.show()



def return_accuracies(feature_review_data,stars,classifier):
    #Split
    training_data_set , testing_data_set , training_labels , testing_labels = train_test_split(feature_review_data, stars, test_size=0.2,random_state=42)

    #Training Model
    model = classifier.fit(training_data_set,training_labels)

    #Predicting Training Data Set and Testing Data Set
    training_labels_p  = model.predict(training_data_set)
    testing_labels_p = model.predict(testing_data_set)
    
    #Returning Testing Accuracy and Training Accuracy
    return (accuracy_score(testing_labels , testing_labels_p)*100.0, accuracy_score(training_labels , training_labels_p)*100.0)


def convert_to_feature_space(list_of_documents,vectorizer):
    #vectorizer=CountVectorizer(min_df=20)
    X=vectorizer.fit_transform(list_of_documents)
    #bigram_vectorizer = CountVectorizer(ngram_range=(1, 2), token_pattern=r'\b\w+\b', min_df=1)
    return X.toarray()

def convert_to_feature_space_hashing(list_of_documents, vectorizer):
    X=vectorizer.transform(list_of_documents)
    return X.toarray()

def plot_new_graph(test_x , train_x , test_y, train_y, classifier_name, feature_extracter):
    plt.figure()
    plt.title(classifier_name+" with Feature Extracter "+feature_extracter)
    plt.plot(train_x, train_y, '.b-');
    plt.plot(test_x, test_y, '.g-');
    plt.xlabel('No of Features');
    plt.ylabel('Accuracy');
    plt.show()


def plot_error(review_text,stars):
    #classifier = GaussianNB()
    classifiers = [ GaussianNB() , MultinomialNB(), BernoulliNB(), LogisticRegression(max_iter=100,multi_class='multinomial',solver='lbfgs') ]
    #classifiers = [ LogisticRegression(max_iter=100,multi_class='multinomial',solver='lbfgs') ] #, MultinomialNB(), BernoulliNB() ]
    #classifiers=[linear_model.BayesianRidge()]
    classifiers_name=["Gaussian Naive Bayes" , "Multinomial Naive Bayes", "Bernoulli Naive Bayes", "Logistic Regression", "Linear Regression"]
    visualize_data=[10,20,40,50,80,100]    
    #Various No of Features
    no_of_features_set=[]
    i=2
    while i <= 100:
        no_of_features_set.append(i)
        i = i + 2


    #Different Kinds of Classifiers 
    index=0
    for classifier in classifiers:
        #1 Gram
        test_x=[]
        test_y=[]
        train_x=[]
        train_y=[]
        #For Different no Of Features
        for no_of_features in no_of_features_set:
            vectorizer = CountVectorizer(max_features=no_of_features)
            features=convert_to_feature_space(review_text,vectorizer)

            #Visualize Data in 2D Space
            if index == 0 and no_of_features in visualize_data:
                plot_2d_differently_rated_reviews(features,stars,"1 Gram "+str(no_of_features)+" Features")

            (test , train ) = return_accuracies( features,stars, classifier)
            test_x.append(no_of_features)
            train_x.append(no_of_features)
            test_y.append(test)
            train_y.append(train)
        
        #Plot Error Vs No of Features
        plot_new_graph(test_x , train_x , test_y, train_y, classifiers_name[index], '1 Gram')
        
        #2 Gram
        test_x=[]
        test_y=[]
        train_x=[]
        train_y=[]
        #For Different no Of Features
        for no_of_features in no_of_features_set:
            vectorizer = CountVectorizer(max_features=no_of_features, ngram_range=(2,2))
            features=convert_to_feature_space(review_text,vectorizer)

            #Visualize Data in 2D Space
            if index == 0 and no_of_features in visualize_data:
                plot_2d_differently_rated_reviews(features,stars,"2 Gram " + str(no_of_features)+" Features")

            (test , train ) = return_accuracies( features,stars, classifier)
            test_x.append(no_of_features)
            train_x.append(no_of_features)
            test_y.append(test)
            train_y.append(train)
        
        #Plot Error Vs No of Features
        plot_new_graph(test_x , train_x , test_y, train_y, classifiers_name[index], '2 Grams')
        
        #3 Gram
        test_x=[]
        test_y=[]
        train_x=[]
        train_y=[]
        #For Different no Of Features
        for no_of_features in no_of_features_set:
            vectorizer = CountVectorizer(max_features=no_of_features,ngram_range=(3,3))
            features=convert_to_feature_space(review_text,vectorizer)

            #Visualize Data in 2D Space
            if index == 0 and no_of_features in visualize_data:
                plot_2d_differently_rated_reviews(features,stars,"3 Gram "+ str(no_of_features)+" Features")

            (test , train ) = return_accuracies( features,stars, classifier)
            test_x.append(no_of_features)
            train_x.append(no_of_features)
            test_y.append(test)
            train_y.append(train)
        
        #Plot Error Vs No of Features
        plot_new_graph(test_x , train_x , test_y, train_y, classifiers_name[index], '3 Grams')
        
        #Tf-IDF
        test_x=[]
        test_y=[]
        train_x=[]
        train_y=[]
        #For Different no Of Features
        for no_of_features in no_of_features_set:
            vectorizer = TfidfVectorizer(max_features=no_of_features)
            features=convert_to_feature_space(review_text,vectorizer)

            #Visualize Data in 2D Space
            if index == 0 and no_of_features in visualize_data:
                plot_2d_differently_rated_reviews(features,stars,"Tf-Idf " + str(no_of_features)+" Features")

            (test , train ) = return_accuracies( features,stars, classifier)
            test_x.append(no_of_features)
            train_x.append(no_of_features)
            test_y.append(test)
            train_y.append(train)
        
        #Plot Error Vs No of Features
        plot_new_graph(test_x , train_x , test_y, train_y, classifiers_name[index], 'Tf-IDF')

        
        #Hashing Vectorizer
        test_x=[]
        test_y=[]
        train_x=[]
        train_y=[]
        #For Different no Of Features
        for no_of_features in no_of_features_set:
            vectorizer = HashingVectorizer(n_features=no_of_features, non_negative=True)
            features=convert_to_feature_space_hashing(review_text,vectorizer)

            #Visualize Data in 2D Space
            if index == 0 and no_of_features in visualize_data:
                plot_2d_differently_rated_reviews(features,stars,"Hashing Vectorizer " + str(no_of_features)+" Features")

            (test , train ) = return_accuracies( features,stars, classifier)
            test_x.append(no_of_features)
            train_x.append(no_of_features)
            test_y.append(test)
            train_y.append(train)
        
        #Plot Error Vs No of Features
        plot_new_graph(test_x , train_x , test_y, train_y, classifiers_name[index], 'Hashing Vectorizer')

        index = index + 1

def convert_to_features_and_test_different_models(review_text,stars):
    #Count Vectorizer 1Grams
    
    #Iterate over all Number of Features
    no_of_features=10
    vectorizer=CountVectorizer(max_features=no_of_features)
    X = vectorizer.fit_transform(review_text)     
    print(X.shape)
     
    #1 Grams and 2 Grams
    vectorizer=CountVectorizer(ngram_range=(1,2),max_features=no_of_features)
    X = vectorizer.fit_transform(review_text)     
    print(X.shape)
    
    #1,2,3 Grams 
    vectorizer=CountVectorizer(ngram_range=(1,3),max_features=no_of_features)
    X = vectorizer.fit_transform(review_text)     
    print(X.shape)

    
    #Tf-Idf
    vectorizer = TfidfVectorizer(max_features=no_of_features)
    X = vectorizer.fit_transform(review_text)     
    print(X.shape)

    #Hashing Vectorizer
    hv = HashingVectorizer(n_features=no_of_features)
    X = hv.transform(review_text)
    print(X.shape)
    print (X.toarray())





