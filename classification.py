from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression



#plots a confusion matrix
def plot_confusion_matrix(cm):
    plt.matshow(cm)
    plt.title('Confusion matrix')
    plt.colorbar()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def get_full_report(training_labels , training_labels_p, testing_labels , testing_labels_p):
    #Primitive Metric Accuracy
    print testing_labels_p
    print ("Accuracy on Testing Data Set:%.3f\n" %(accuracy_score(testing_labels , testing_labels_p)*100.0))
    print ("Accuracy on Training Data Set:%.3f\n"  %(accuracy_score(training_labels , training_labels_p)*100.0 ))
    
    #Precision , Recall , F1-Score 
    class_labels=['1','2','3','4','5']
    print(classification_report(testing_labels,testing_labels_p,target_names=class_labels))
    
    #Confusion Matrix
    cm=confusion_matrix(testing_labels , testing_labels_p)
    plot_confusion_matrix(cm)

def run_classifier(feature_review_data,stars,classifier,no):
    (rows,cols)=feature_review_data.shape
    rows_train = 4*rows/5

    #Data Set Division
    #training_data_set = feature_review_data[0:rows_train]
    #testing_data_set = feature_review_data[rows_train:rows]

    #Data Set Labels
    #training_labels = stars[0:rows_train]
    #testing_lables = stars[rows_train:rows]

    training_data_set , testing_data_set , training_labels , testing_labels = train_test_split(feature_review_data, stars, test_size=0.2,random_state=42)

    #Training Model
    model = classifier.fit(training_data_set,training_labels)

    #Predicting Training Data Set and Testing Data Set
    training_labels_p  = model.predict(training_data_set)
    testing_labels_p = model.predict(testing_data_set)
    
    #Evaluating Accuracy of Model
    training_accuracy = ( training_labels_p == training_labels ).sum()
    training_accuracy = ( float(training_accuracy) /float(rows_train) )*100.0

    testing_accuracy = ( testing_labels_p == testing_labels ).sum()
    testing_accuracy = ( float(testing_accuracy) /float( rows - rows_train ) ) * 100.0
    

    if no == 1:
        print ("Gaussian Naive Bayes\n")
    elif no == 2:
        print ("Multinomial Naive Bayes\n")
    elif no == 3:
        print ("Bernoulli Naive Bayes\n")
    elif no == 4:
        print ("Logistic Regression\n")

    get_full_report(training_labels , training_labels_p, testing_labels , testing_labels_p)
    
def run_all_classifiers(feature_review_data, stars):
    #run_classifier(feature_review_data, stars, GaussianNB(), 1)
    run_classifier(feature_review_data, stars, MultinomialNB(), 2)
    run_classifier(feature_review_data,stars,LogisticRegression(max_iter=100,multi_class='multinomial',solver='lbfgs'),4)
    #run_classifier(feature_review_data, stars, BernoulliNB(), 3)

 




