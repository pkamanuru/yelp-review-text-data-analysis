from sklearn.decomposition import PCA
import pylab as plt
colors=['b','c','y','m','r','g']
labels=['Rating 0','Rating 1','Rating 2','Rating 3','Rating 4','Rating 5']

def plot_2d_clustering_output(x,y,color_value):
    plt.scatter(x,y,color=colors[color_value],label=labels[color_value])

def plot_3d_clustering_output(x,y,z,color_value):
    plt.scatter(x,y,z,color=colors[color_value],label=labels[color_value])

def plot_2d_differently_rated_reviews(features_review_data,stars):
    plt.figure()
    pca=PCA(n_components=2).fit(features_review_data)
    pca_2d=pca.transform(features_review_data)
    
    #Splitting Output into x and y Coordinates
    x=list(pca_2d[:,0])
    y=list(pca_2d[:,1])
    for rating in range(1,6):
        #Subset Indices of those with stars rating
        stars_i = [review_index for review_index in range(len(stars)) if stars[review_index] == rating]
        
        #Fetching those Corresponding Points
        x_subset = [x[i] for i in stars_i]
        y_subset = [y[i] for i in stars_i]
        plot_2d_clustering_output(x_subset,y_subset,rating)
    plt.show()

def plot_3d_differently_rated_reviews(features_review_data,stars):
    plt.show()
    pca=PCA(n_components=3).fit(features_review_data)
    pca_3d=pca.transform(features_review_data)
    
    #Splitting Output into x and y Coordinates
    x=list(pca_3d[:,0])
    y=list(pca_3d[:,1])
    z=list(pca_3d[:,2])

    for rating in range(1,6):
        #Subset Indices of those with stars rating
        stars_i = [review_index for review_index in range(len(stars)) if stars[review_index] == rating]
        
        #Fetching those Corresponding Points
        x_subset = [x[i] for i in stars_i]
        y_subset = [y[i] for i in stars_i]
        z_subset = [z[i] for i in stars_i]

        plot_3d_clustering_output(x_subset,y_subset,z_subset,rating)






